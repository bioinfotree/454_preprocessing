### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Aug  3 15:24:25 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:


extern ../phase_1/CDNA1_454_cleaned_newbler.fastq as FASTQ1

# links
.SECONDEXPANSION:
cleaned%.fastq: $$(FASTQ%)
	ln -sf $< $@

.IGNORE: no_oligo1.fastq
no_oligo%.fastq: cleaned%.fastq
	trim_blast_short -t fastq -l CGTATCGCCTCCCTCGCGCCATCAG -l CTATGCGCCTTGCCAGCCCGCTCAG <$< >$@





.PHONY: test
test:
	@echo 


ALL   += 

INTERMEDIATE += 

CLEAN += 


######################################################################
### phase_1.mk ends here
