### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Aug  3 15:24:25 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

extern ../phase_1/CDNA$(L1IDX)_cl/CDNA$(L1IDX)_454.cleaned.fasta.x.in_newbler as CDNA$(L1IDX)_CLEANED_NB_FASTA
extern ../phase_1/CDNA$(L2IDX)_cl/CDNA$(L2IDX)_454.cleaned.fasta.x.in_newbler as CDNA$(L2IDX)_CLEANED_NB_FASTA

extern ../phase_1/CDNA$(L1IDX)-$(L2IDX)_454_cleaned.fasta as CLEANED_FASTA
extern ../phase_1/CDNA$(L1IDX)-$(L2IDX)_454_cleaned.fasta.qual as CLEANED_QUAL
extern ../phase_1/CDNA$(L1IDX)-$(L2IDX)_454_cleaned.strain as CLEANED_STRAIN
extern ../phase_1/CDNA$(L1IDX)-$(L2IDX)_454_cleaned.xml as CLEANED_XML
extern ../phase_1/CDNA$(L1IDX)-$(L2IDX)_454_cleaned_newbler.fasta as CLEANED_NB_FASTA
extern ../phase_1/CDNA$(L1IDX)-$(L2IDX)_454_cleaned_newbler.fasta.qual as CLEANED_NB_QUAL


CDNA$(L1IDX)_cleaned_nb.fasta: $(CDNA$(L1IDX)_CLEANED_NB_FASTA)
	ln -sf $< $@
CDNA$(L2IDX)_cleaned_nb.fasta: $(CDNA$(L2IDX)_CLEANED_NB_FASTA)
	ln -sf $< $@

cleaned.fasta: $(CLEANED_FASTA)
	ln -sf $< $@
cleaned.qual: $(CLEANED_QUAL)
	ln -sf $< $@
cleaned.strain: $(CLEANED_STRAIN)
	ln -sf $< $@
cleaned.xml: $(CLEANED_XML)
	ln -sf $< $@
cleaned_nb.fasta: $(CLEANED_NB_FASTA)
	ln -sf $< $@
cleaned_nb.qual: $(CLEANED_NB_QUAL)
	ln -sf $< $@




cleaned_nb.info: cleaned_nb.fasta cleaned_nb.qual
	_comment () { echo -n ""; }; \
	count_fasta -i 50 $< > $@ && \
	fastalen $< | \
	stat_base --mean 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "mean_length",$$0; } \
	}' >> $@ && \
	fastalen $< | \
	stat_base --stdev 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "standard_deviation_length",$$0; } \
	}' >> $@ && \
	fastalen $< | \
	stat_base --median 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "median_length",$$0; } \
	}' >> $@ && \
	qual_mean --total < $^2  | \
	bawk '!/^[$$,\#+]/ { \
	{ print "mean_quality",$$0; } \
	}' >> $@





# join together lengths for every fasta
# be sure that the longest columns is the first (transpose, sort by line length, restranspose)
cleaned_nb_len_distr.pdf: cleaned_nb.fasta CDNA$(L2IDX)_cleaned_nb.fasta CDNA$(L1IDX)_cleaned_nb.fasta
	paste <(fastalen $< | cut -f2) \
	<(fastalen $^2 | cut -f2) \
	<(fastalen $^3 | cut -f2) | \
	transpose --skip-missing | \
	bsort -V | \
	transpose --skip-missing | \
	this_distrib_plot --type histogram --remove-na --bin-size=25 --title="Length distribution of cleaned reads" --xlab="length" --ylab="reads (#)" --legend-label="total","cDNA$(L2IDX)","cDNA$(L1IDX)" --color=black,blue,red --output $@ 1 2 3





.PHONY: test
test: 
	echo


# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += cleaned.fasta \
	 cleaned.qual \
	 cleaned.strain \
	 cleaned.xml \
	 cleaned_nb.fasta \
	 cleaned_nb.qual \
	 CDNA$(L1IDX)_cleaned_nb.fasta \
	 CDNA$(L2IDX)_cleaned_nb.fasta \
	 cleaned_nb.info \
	 cleaned_nb_len_distr.pdf


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += 




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += cleaned.fasta \
	 cleaned.qual \
	 cleaned.strain \
	 cleaned.xml \
	 cleaned_nb.fasta \
	 cleaned_nb.qual \
	 CDNA$(L1IDX)_cleaned_nb.fasta \
	 CDNA$(L2IDX)_cleaned_nb.fasta \
	 cleaned_nb.info \
	 cleaned_nb_len_distr.pdf



######################################################################
### phase_1.mk ends here
