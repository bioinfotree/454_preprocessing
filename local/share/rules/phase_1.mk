### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Aug  3 15:24:25 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

# need qualfa2fq_unwrap from bin
#context prj/transposon

# variables
L1IDX ?=
L2IDX ?=

SFF$(L1IDX)_OLD ?=
SFF$(L1IDX)_NEW ?=

SFF$(L2IDX)_OLD ?=
SFF$(L2IDX)_NEW ?=

ADAPTOR_DB ?=
MINI_ADAPTOR_DB ?=



# Copy files for GNU MAKE STANDARD LIBRARY
__gmsl: rules.mk
	RULES_PATH=`readlink -f $<`; \
	GMSL_ABS_PATH="`dirname $$RULES_PATH`/GMSL/$@"; \
	cp $$GMSL_ABS_PATH $@; \
	echo "Copied $$GMSL_ABS_PATH.."


gmsl: rules.mk __gmsl
	RULES_PATH=`readlink -f $<`; \
	GMSL_ABS_PATH="`dirname $$RULES_PATH`/GMSL/$@"; \
	cp $$GMSL_ABS_PATH $@; \
	echo "Copied $$GMSL_ABS_PATH.."

include __gmsl



# links
SFF$(L1IDX)_OLD: $(SFF$(L1IDX)_OLD)
	ln -sf $< $@
SFF$(L1IDX)_NEW: $(SFF$(L1IDX)_NEW)
	ln -sf $< $@
SFF$(L2IDX)_OLD: $(SFF$(L2IDX)_OLD)
	ln -sf $< $@
SFF$(L2IDX)_NEW: $(SFF$(L2IDX)_NEW)
	ln -sf $< $@

# join sffs
SFF%_TOTAL.sff: SFF%_OLD SFF%_NEW
	sfffile -o $@ $< $^2


flower_CDNA$(L1IDX)_summarize: CDNA$(L1IDX).sff
	flower --summarize=$@ $<

flower_CDNA$(L2IDX)_summarize: CDNA$(L2IDX).sff
	flower --summarize=$@ $<

flower_CDNA$(L1IDX)_histogram: CDNA$(L1IDX).sff
	flower --histogram=$@ $<

flower_CDNA$(L2IDX)_histogram: CDNA$(L2IDX).sff
	flower --histogram=$@ $<

# extract fasta and fasta.qual
.IGNORE: CDNA1_untagged.fasta CDNA2_untagged.fasta
CDNA%_untagged.fasta: SFF%_TOTAL.sff
	sff_extract --fasta --out_basename=$(basename $@) $<

CDNA%_untagged.fasta.qual: CDNA%_untagged.fasta
	touch $@

CDNA%_untagged.xml: CDNA%_untagged.fasta
	touch $@


# tags IDs of sequences with a prefix
# use macro
CDNA%.fasta: CDNA%_untagged.fasta
	bawk -v tag=$(L$*IDX) ' \
	!/^[$$,\#+]/ { \
	if ( $$1 ~ /^>\w{14}/) \
	{ \
	split($$1,a,">"); \
	printf ">%s_%s\n", tag, a[2]; \
	} \
	else \
	{ print $$0; } \
	}' $< > $@



CDNA%.fasta.qual: CDNA%_untagged.fasta.qual
	bawk -v tag=$(L$*IDX) ' \
	!/^[$$,\#+]/ { \
	if ( $$1 ~ /^>\w{14}/) \
	{ \
	split($$1,a,">"); \
	printf ">%s_%s\n", tag, a[2]; \
	} \
	else \
	{ print $$0; } \
	}' $< > $@


# add tag to xml traceninfo
CDNA%.xml: CDNA%_untagged.xml
	add2traceinfo --tag $(L$*IDX) < $< > $@


# CDNA$(L1IDX).flower.fasta: SFF$(L1IDX)_TOTAL.sff
# 	flower --fasta=$@ $<

# CDNA$(L1IDX).sff2fastq.fastq: SFF$(L1IDX)_TOTAL.sff
# 	sff2fastq -n -o $@ $<

# CDNA$(L1IDX).pyrobayes.fastq: SFF$(L1IDX)_TOTAL.sff
# 	PyroBayes -r CDNA$(L1IDX)_ -o $@ -i $<



# transform into fastq
CDNA%.fastq: CDNA%.fasta CDNA%.fasta.qual
	fastaqual2fastq --fasta $< --qual $^2 > $@


# get report statistics from fastqc
CDNA%_fastqc.zip: CDNA%.fastq
	!threads
	fastqc --format fastq --noextract --threads $$THREADNUM $<

# prepare preprocessest configuration file
process_454.conf: $(ADAPTOR_DB) $(MINI_ADAPTOR_DB) $(REPEAT_LIB)
	touch $@ && \
	echo "adaptor_db=\"$<\"" >> $@ && \
	echo "mini_adaptor_db=\"$^2\"" >> $@

# clean sequences using est_process module into est2assembly package
.NOTPARALLEL: CDNA$(L1IDX)_cl.flag CDNA$(L2IDX)_cl.flag
CDNA%_cl.log: CDNA%.fasta CDNA%.fasta.qual process_454.conf
	!threads
	_comment () { echo -n ""; }; \
	DIR_NAME="$(basename $@)"; \
	_comment "make directories"; \
	mkdir -p $$DIR_NAME && cd $$DIR_NAME && \
	preprocessest --dir ../ --technology 454 --project $(call first,$(call split,_,$(basename $@))) --nosff --nomito --noecoli --nordna --norepeats --thread $$THREADNUM --backuptmp --deletetmp --archivelog --config ../$^3 --strain $(call first,$(call split,_,$(basename $@))) ../$< >../$@ 2>&1 && \
	cd .. \
	_comment "all done flag"; \
	touch $@






# transform into fastq using sequence files for newbler. Their sequences are really cut
CDNA%_454_cleaned_newbler.fastq: CDNA%_cl.log
	fastaqual2fastq --fasta $(basename $<)/$(call first,$(call split,_,$(basename $<)))_454.cleaned.fasta.x.in_newbler --qual $(basename $<)/$(call first,$(call split,_,$(basename $<)))_454.cleaned.fasta.x.in_newbler.qual > $@

# get report statistics from fastqc
CDNA%_454_cleaned_newbler_fastqc.zip: CDNA%_454_cleaned_newbler.fastq
	!threads
	fastqc --format fastq --noextract --threads $$THREADNUM $<

# get statistic about GC and total length
CDNA%_454_cleaned_newbler.info: CDNA%_cl.log CDNA%_454_cleaned_newbler.fastq
	_comment () { echo -n ""; }; \
	count_fasta -i 50 $(basename $<)/$(call first,$(call split,_,$(basename $<)))_454.cleaned.fasta.x.in_newbler > $@ && \
	fastalen $(basename $<)/$(call first,$(call split,_,$(basename $<)))_454.cleaned.fasta.x.in_newbler | \
	stat_base --mean 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "mean_length",$$0; } \
	}' >> $@ && \
	fastalen $(basename $<)/$(call first,$(call split,_,$(basename $<)))_454.cleaned.fasta.x.in_newbler | \
	stat_base --stdev 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "standard_deviation_length",$$0; } \
	}' >> $@ && \
	fastalen $(basename $<)/$(call first,$(call split,_,$(basename $<)))_454.cleaned.fasta.x.in_newbler | \
	stat_base --median 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "median_length",$$0; } \
	}' >> $@ && \
	qual_mean --total < $(basename $<)/$(call first,$(call split,_,$(basename $<)))_454.cleaned.fasta.x.in_newbler.qual | \
	bawk '!/^[$$,\#+]/ { \
	{ print "mean_quality",$$0; } \
	}' >> $@






# merge cleaned files
## mira
CDNA$(L1IDX)-$(L2IDX)_454_cleaned.fasta: CDNA$(L1IDX)_cl.log CDNA$(L2IDX)_cl.log
	cat $(basename $<)/$(call first,$(call split,_,$(basename $<)))_454.cleaned.fasta.x $(basename $^2)/$(call first,$(call split,_,$(basename $^2)))_454.cleaned.fasta.x  > $@

CDNA$(L1IDX)-$(L2IDX)_454_cleaned.fasta.qual: CDNA$(L1IDX)_cl.log CDNA$(L2IDX)_cl.log
	cat $(basename $<)/$(call first,$(call split,_,$(basename $<)))_454.cleaned.fasta.x.qual $(basename $^2)/$(call first,$(call split,_,$(basename $^2)))_454.cleaned.fasta.x.qual > $@

CDNA$(L1IDX)-$(L2IDX)_454_cleaned.strain: CDNA$(L1IDX)_cl.log CDNA$(L2IDX)_cl.log
	cat $(basename $<)/$(call first,$(call split,_,$(basename $<)))_454.cleaned.fasta.strain $(basename $^2)/$(call first,$(call split,_,$(basename $^2)))_454.cleaned.fasta.strain > $@

CDNA$(L1IDX)-$(L2IDX)_454_cleaned.xml: CDNA$(L1IDX)_cl.log CDNA$(L2IDX)_cl.log
	merge_trace_xmls $(basename $<)/$(call first,$(call split,_,$(basename $<)))_454.cleaned.fasta.x.xml $(basename $^2)/$(call first,$(call split,_,$(basename $^2)))_454.cleaned.fasta.x.xml -o $@

## newbler trimmed
CDNA$(L1IDX)-$(L2IDX)_454_cleaned_newbler.fasta: CDNA$(L1IDX)_cl.log CDNA$(L2IDX)_cl.log
	cat $(basename $<)/$(call first,$(call split,_,$(basename $<)))_454.cleaned.fasta.x.in_newbler $(basename $^2)/$(call first,$(call split,_,$(basename $^2)))_454.cleaned.fasta.x.in_newbler > $@

CDNA$(L1IDX)-$(L2IDX)_454_cleaned_newbler.fasta.qual: CDNA$(L1IDX)_cl.log CDNA$(L2IDX)_cl.log
	cat $(basename $<)/$(call first,$(call split,_,$(basename $<)))_454.cleaned.fasta.x.in_newbler.qual $(basename $^2)/$(call first,$(call split,_,$(basename $^2)))_454.cleaned.fasta.x.in_newbler.qual > $@







.PHONY: test
test:
	@echo CDNA$(L1IDX)-$(L2IDX)_454_cleaned_newbler.fasta.qual
	@echo $(SFF$(L2IDX)_OLD)


# Standard Phony Targets for Users.

# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += CDNA$(L1IDX).fasta \
	 CDNA$(L1IDX).fasta.qual \
	 CDNA$(L2IDX).fasta \
	 CDNA$(L2IDX).fasta.qual \
	 CDNA$(L1IDX)-$(L2IDX)_454_cleaned.fasta \
	 CDNA$(L1IDX)-$(L2IDX)_454_cleaned.fasta.qual \
	 CDNA$(L1IDX)-$(L2IDX)_454_cleaned.strain \
	 CDNA$(L1IDX)-$(L2IDX)_454_cleaned.xml \
	 CDNA$(L1IDX)-$(L2IDX)_454_cleaned_newbler.fasta \
	 CDNA$(L1IDX)-$(L2IDX)_454_cleaned_newbler.fasta.qual \
	 CDNA$(L1IDX)_454_cleaned_newbler_fastqc.zip \
	 CDNA$(L2IDX)_454_cleaned_newbler_fastqc.zip \
	 CDNA$(L1IDX)_454_cleaned_newbler.info \
	 CDNA$(L2IDX)_454_cleaned_newbler.info




# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += 




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += __gmsl \
	 SFF$(L1IDX)_OLD \
	 SFF$(L1IDX)_NEW \
	 SFF$(L2IDX)_OLD \
	 SFF$(L2IDX)_NEW \
	 SFF$(L1IDX)_TOTAL.sff \
	 SFF$(L2IDX)_TOTAL.sff \
	 CDNA$(L1IDX).fasta \
	 CDNA$(L1IDX).fasta.qual \
	 CDNA$(L1IDX).xml \
	 CDNA$(L2IDX).fasta \
	 CDNA$(L2IDX).fasta.qual \
	 CDNA$(L2IDX).xml \
	 CDNA$(L1IDX).fastq \
	 CDNA$(L2IDX).fastq \
	 CDNA$(L1IDX)_fastqc.zip \
	 CDNA$(L2IDX)_fastqc.zip \
	 CDNA$(L1IDX)_cl.flag \
	 CDNA$(L2IDX)_cl.flag \
	 process_454.conf \
	 CDNA$(L1IDX)-$(L2IDX)_454_cleaned.fasta \
	 CDNA$(L1IDX)-$(L2IDX)_454_cleaned.fasta.qual \
	 CDNA$(L1IDX)-$(L2IDX)_454_cleaned.strain \
	 CDNA$(L1IDX)-$(L2IDX)_454_cleaned.xml \
	 CDNA$(L1IDX)-$(L2IDX)_454_cleaned_newbler.fasta \
	 CDNA$(L1IDX)-$(L2IDX)_454_cleaned_newbler.fasta.qual \
	 CDNA$(L1IDX)_untagged.fasta.qual \
	 CDNA$(L1IDX)_untagged.xml \
	 CDNA$(L2IDX)_untagged.fasta.qual \
	 CDNA$(L2IDX)_untagged.xml \
	 CDNA$(L1IDX)_454_cleaned_newbler_fastqc.zip \
	 CDNA$(L2IDX)_454_cleaned_newbler_fastqc.zip \
	 CDNA$(L1IDX)_454_cleaned_newbler.info \
	 CDNA$(L2IDX)_454_cleaned_newbler.info \
	 CDNA$(L1IDX)_cl \
	 CDNA$(L2IDX)_cl



######################################################################
### phase_1.mk ends here
