######################################################################
#######
####### Prepare the straindata file for mira given a fasta input file
#######
######################################################################
#
# USAGE:
# sh make_strain_names.sh [INPUT_FILE.fasta] [STRAIN_NAME]
 


# strainname: name of the strain which was sequenced
strainname="CDNA3"
# $2 means the first argument from command line. In this case: [STRAIN_NAME]
strainname=$2
################################


# make file with strainnames
echo "Creating file with strain names for copied reads (this may take a while)."
grep ">" $1 | cut -f 1 -d ' ' | sed -e 's/>//' -e "s/$/ ${strainname}/" > ${strainname}_straindata_in.txt

