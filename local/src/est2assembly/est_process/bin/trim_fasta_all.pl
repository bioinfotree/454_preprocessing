#!/usr/bin/perl -w
package trim_fasta_all;
use warnings;
use strict;
use Data::Dumper;
our $VERSION = '1.0';

#30MAR09: Added GC/AT ratio check at proportion cutoff

=head1 NAME

trim_fasta_all.pl - removes sequences from a FASTA file 

=head1 VERSION

 Version 0.2

=head1 SYNOPSIS

trim_fasta_all.pl [options] <infiles>

removes sequences from a FASTA file. See perldoc for more info.

	'i|fa|fasta=s'    => FASTA file to trim. You can also give multiples as arguments without any -i/-fa option.
	'outfile:s'	=> Optionally, the name of the trimmed outfile
	'blastfile:s'	=> BLASTFILE to retrieve sequences from
	'query'		=> Only grab BLAST queries; other both queries and hits
	'evalue=s'	=> Evalue cut-off for blastfile
	'c|character=s' => Characters to look for. If present, remove sequence.
	'le|length=i'   => Number of minimum characters
	'p|proportion'  => Discard sequences for which a mononucleotide frequency exceeds this proportion 
	'x'             => Do not include the Xx characters when calculating size of sequence
	'npl'           => Do not include these characters when calculating size: NPLnpl
	'lc|lowercase'  => Do not include lowercase characters when calculating size of sequence (e.g. to not include low quality bases)
	'id|idfile=s'   => A second FASTA file containing IDs to remove from FASTA file
	'descr'		=> For above: search description line instead of primary id.
	'ci'		=> Case insensitivity for above two options
	'invert'	=> Invert match (invert output filenames)
	'log'           => Keep a log file
	'df'            => Do not write discarded sequences (less IO)
    'solq'          => Input is FASTQ (Solexa 1.3-1.4)
    'sanq'          => Input is FASTQ (Sanger)


=head1 DESCRIPTION

Processes file (-fa) when certain character(s) are present (-c); or a list of IDs is provided (-id); or a certain length-cut off is not satisfied (-le); or a proportion of nucleotide frequence can be specified (-p) instead. The -log option produces a log file reporting what happened to each sequence 
The option to not include Xs and/or NPLs and/or lower-case characters in the cut-off calculation is forced with -x and/or -npl and/or -lc respectively.
Uses BioPerl. A disk-friendly function (-df) prevents the FASTA file of discarded sequences of being written.

=head1 AUTHORS

 Alexie Papanicolaou 1 2

	1 Max Planck Institute for Chemical Ecology, Germany
	2 Centre for Ecology and Conservation, University of Exeter, UK
	alexie@butterflybase.org

=head1 DISCLAIMER & LICENSE

This software is released under the GNU General Public License version 3 (GPLv3).
It is provided "as is" without warranty of any kind.
You can find the terms and conditions at http://www.opensource.org/licenses/gpl-3.0.html.
Please note that incorporating the whole software or parts of its code in proprietary software
is prohibited under the current license.

=head1 BUGS & LIMITATIONS

None known so far.

=cut

use Bio::SeqIO;
use Bio::SearchIO;
use Time::Progress;
use Getopt::Long;
use Pod::Usage;
$| = 1;
my (
	 $character, $infile,   $length_cutoff, $xmask, $nplmask, $ci, $blastfile, $evalue_cutoff,
	 $lcmask,    $prop_cutoff, $idfile,        $log,   $logfile, $invert, $sangerfastq, $only_query, $user_outfile,
	 $df,        %ids,         $help,          $convert2uc, $descr_flag, $solexafastq, $search_accession, $seq_search
);
GetOptions(
			'i|fa|fasta=s'    => \$infile,
			'blastfile=s'	=> \$blastfile,
			'query'		=> \$only_query,
			'evalue=s'	=> \$evalue_cutoff,
			'c|character=s' => \$character,
			'le|length=i'   => \$length_cutoff,
			'p|proportion=f'  => \$prop_cutoff,
			'x'             => \$xmask,
			'uc|uppercase'  => \$convert2uc,
			'npl'           => \$nplmask,
			'lc|lowercase'  => \$lcmask,
			'ids|idfile=s'   => \$idfile,
			'descr'		=> \$descr_flag,
			'invert'	=> \$invert,
			'ci'		=> \$ci,
			'log'           => \$log,
			'df'            => \$df,
			'h|help'        => \$help,
			'solq'		=> \$solexafastq,
			'sanq'       => \$sangerfastq,
			'seq'		=> \$seq_search,
			'outfile:s'	=> \$user_outfile,
			#'accessions'=> \$search_accession,
);
if ($help) { pod2usage; }
my @infiles=@ARGV;

unless ( ($infile && -f $infile) || @infiles) {
	print "Failed to provide or find input file\n";pod2usage;
}
unless ( $character || $length_cutoff || $prop_cutoff || $idfile || $blastfile) {
	die("Nothing to do!\n");
}
unless ($evalue_cutoff){$evalue_cutoff=1;}

if ($idfile && -s $idfile) {
	my $pattern;
	if ($descr_flag){$pattern='^\s*\S+\s+(.+)$';}
	else{$pattern='^\s*(\S+)\s*';}
	my @test_lines=`head $idfile`;
	foreach my $test (@test_lines){if ($test=~/^>/){$pattern="Bio::SeqIO";}}
	print "Building hash from $idfile with $pattern\n";
	my $flag;

	if ($pattern eq "Bio::SeqIO"){
		my $id_obj=new Bio::SeqIO( -file => $idfile,-format => "fasta" );
		while ( my $seq = $id_obj->next_seq() ) {
			if ($seq_search){$ids{$seq->seq()}=1;}
			elsif ($descr_flag){$ids{$seq->description()}=1;}
			else{$ids{$seq->id()}=1;}
			$flag=1 if !$flag;
		}
	}
	else{
		open( IN, $idfile ) || die();
		while ( my $line = <IN> ) {
			if ($ci){
				if ( $line =~ /$pattern/i ) {
					$ids{$1} = 1;
					$flag = 1 if !$flag;
				}
			}
			else {
				if ( $line =~ /$pattern/ ) {
					$ids{$1} = 1;
					$flag = 1 if !$flag;
				}
			}
		}
		close(IN);
	}

	if (!$flag){die "Failed to get list of IDs to extract...\n";}
	else {
		print "Hash presence of $idfile verified\n";
		if ($log) { print LOG "Hash presence of $idfile verified\n"; }
	}
}elsif ($idfile){warn "File $idfile is not existent or empty!\n";}

if ($blastfile && -s $blastfile){
	print "Building HASH for queries and hits from $blastfile...\n";
	my $blast = Bio::SearchIO->new( -format => "blast", -file => $blastfile, -signif=>$evalue_cutoff );
	while ( my $result = $blast->next_result() ) {
		my $query_id = $result->query_name();
		$ids{$query_id}=1;
		next if $only_query;
		unless ($result->num_hits() && $result->num_hits()>=1){next;}
		while ( my $hit = $result->next_hit ) {
			my $hit_id = $hit->name();
			$ids{$hit_id}=1;
		}
	}
}
if ($infile){&process($infile);}
if (@infiles){
	foreach my $file (@infiles){
		&process($file);
	}
}
#####################################################
sub process ($){
my $fastafile=shift;
print "Processing... $fastafile\n";
my $fastafiletrim="$fastafile.trim";
$fastafiletrim=$user_outfile if $user_outfile;
my $fastafilediscard="$fastafile.discard";
$fastafilediscard=$user_outfile.".discard" if $user_outfile;
my ($filein,$fileout,$fileout2,$number);

if ($solexafastq){
 $number = `grep -c '^\@' $fastafile`;
 $filein  = new Bio::SeqIO( -file => $fastafile,         -format => "fastq-solexa" );
 $fileout = new Bio::SeqIO( -file => ">$fastafiletrim", -format => "fastq-solexa" );
 $fileout2 = new Bio::SeqIO( -file => ">$fastafilediscard", -format => "fastq-solexa" );
}elsif($sangerfastq){
 $number = `grep -c '^\@' $fastafile`;
 $filein  = new Bio::SeqIO( -file => $fastafile,         -format => "fastq" );
 $fileout = new Bio::SeqIO( -file => ">$fastafiletrim", -format => "fastq" );
 $fileout2 = new Bio::SeqIO( -file => ">$fastafilediscard", -format => "fastq" );
}else{
 $number = `grep -c '^>' $fastafile`;
 $filein  = new Bio::SeqIO( -file => $fastafile,         -format => "fasta" );
 $fileout = new Bio::SeqIO( -file => ">$fastafiletrim", -format => "fasta" );
 $fileout2 = new Bio::SeqIO( -file => ">$fastafilediscard", -format => "fasta" );
}
chomp($number);
if ($log) {
	$logfile = $fastafile . ".trim.log";
	open( LOG, ">$logfile" );
}
if ($log) { print LOG "FASTA $fastafile contained $number sequences\n"; }
my ( $counter, $empty, $discard, $trim );
my $timer = new Time::Progress;
$timer->attr( min => 0, max => $number );
$timer->restart;

# For each sequence
while ( my $seq = $filein->next_seq() ) {
	$counter++;
	if ( $counter =~ /0000$/ ) {
		print $timer->report( "eta: %E min, %40b %p\r", $counter );
	}
	my $sequence;
	my $id = $seq->id();
	if ($seq_search){$sequence=$seq->seq();}
	# trim if given an ID file
	if ($idfile || $blastfile) {
		if ($sequence && $seq_search){
			if ($ids{$sequence}){
	                        if ($ids{$sequence}==1) {
	                                unless ($df && !$invert) { $fileout2->write_seq($seq); }
        	                        $discard++;
                	                if ($log) {
                        	                print LOG "Sequence $id discarded because the Sequence was found in $idfile\n";
                                	}
	                                #make sure we don't get it twice
        	                        $ids{$sequence} =2;
                	                next;
                        	}
			}
			else {next;} 
		}
		elsif ( $ids{$id}){ 
			if ($ids{$id}==1) {
				unless ($df && !$invert) { $fileout2->write_seq($seq); }
				$discard++;
				if ($log) {
					print LOG "Sequence $id discarded because the ID was found in $idfile\n";
				}
				#make sure we don't get it twice
				$ids{$id} =2;
				next;
			}
			else {next;}	# if id exists multiple times don't write it in any file.
		}
	}
	$sequence = $seq->seq() if !$sequence;
	if ($sequence) {
		if ($xmask)   { $sequence =~ s/[X]//ig; }
		if ($nplmask) { $sequence =~ s/[NPL]//ig; }
		if ($lcmask)  { $sequence =~ s/[a-z]//g; }
		my $length = length($sequence);

		# trim if given a character(s)
		if ($character) {
			if ( $sequence =~ /[$character]/ ) {
				unless ($df && !$invert) { $fileout2->write_seq($seq); }
				$discard++;
				if ($log) {
					print LOG "Sequence $id discarded because character $character was found\n";
				}
				next;
			}
		}

		#trim if given a length cutoff
		if ($length_cutoff) {
			if ( !$length || $length < $length_cutoff ) {
				unless ($df && !$invert) { $fileout2->write_seq($seq); }
				$discard++;
				if ($log) {
					print LOG
"Sequence $id discarded because length $length was smaller than cutoff $length_cutoff\n";
				}
				next;
			}
		}

		#trim if given a proportion of A/T/C/G
		if ($prop_cutoff) {
			my $As = ( $sequence =~ tr/A// );
			my $Ts = ( $sequence =~ tr/T// );
			my $Cs = ( $sequence =~ tr/C// );
			my $Gs = ( $sequence =~ tr/G// );
			my $propA = ( $As / $length );
			my $propT = ( $Ts / $length );
			my $propC = ( $Cs / $length );
			my $propG = ( $Gs / $length );
			my $GCratio = $propG+$propC;
			my $ATratio = 1-$GCratio;
			if (    $propA > $prop_cutoff
				 || $propT > $prop_cutoff
				 || $propG > $prop_cutoff
				 || $propC > $prop_cutoff
				 || $ATratio > $prop_cutoff
				 || $GCratio > $prop_cutoff
				 )
			{
				unless ($df && !$invert) { $fileout2->write_seq($seq); }
				$discard++;
				if ($log) {
					print LOG
"Sequence $id discarded because of one nucleotide proportion (A:$propA; T:$propT; G:$propG; C:$propC higher than cutoff $prop_cutoff\n";
				}
				next;
			}
		}

		#next has taken care of discards.
		$trim++;
		if ($convert2uc) {
			$seq->seq( uc($sequence) );
		}
		$fileout->write_seq($seq) unless ($df && $invert);
	}    #end if $sequence
	else {
		$empty++;
		if ($log) {
			print LOG "Sequence $id discard because it was empty\n";
		}
	}
}
if ( !$empty )   { $empty   = int(0); }
if ( !$discard ) { $discard = int(0); }
if ( !$trim )    { $trim    = int(0); }


if ($invert){
	system ("mv -i $fastafiletrim tmpfile");
	system ("mv $fastafilediscard $fastafiletrim");
	system ("mv tmpfile $fastafilediscard");
	my $temp=$trim;$trim=$discard;$discard=$temp;
}
unless (-s "$fastafilediscard"){unlink "$fastafilediscard";}
my $elapsed=$timer->report("%L");print "\nTime elapsed: $elapsed min.\nDone, $empty were empty and an additional $discard were discarded. Kept $trim as $fastafiletrim\n";
if ($log) {
	print LOG
"\n$empty were empty and an additional $discard were discarded. Kept $trim as $fastafiletrim\n";
}
close(LOG);
}
print "\n";
