#!/usr/bin/env python

# fastaqual2fastq.py --- 
# 
# Filename: fastaqual2fastq.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Tue Oct 18 10:14:42 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
#


# Prerequisites:
#      conversion:
#      standard.fasta + standard fasta.qual in Phred format to standard fastq-sanger


#      input:
#           - quality files must contain at least the same ID as
#             in the FASTA file. The FASTA file decides the number of records
#             in the output.

#           - The records in FASTA files and quality can be in a different order.
#             More performace if first ordered with the header? -> ORDER CHECK
#             e.g. the records are in the same order

#           - record must have the same sequence length.
#           - I have to control identities of "ids" in blocks "fasta" and "qual"
#             to identify them as identical, or should I trust the order and
#             place, as a constraint, the length of the string "fasta" and
#             "qual"?



#      output:
#           to std-out

#           header: can or not include header header lines that defines FASTQ
#                   format like #Solexa FASTQ 1.0 or #Illumina FASTQ 1.3 or
#                   #Sanger FASTQ


#           1  line: "@record identifier"
#                     - no length limit 
#                     - comment + annotation permitted
#           2  line: sequence
#                    - no wrapped (but can be)
#                    - no explicit limitation to characters expected
#                    - restriction to the IUPAC single letter codes for
#                    (ambiguous) DNA or RNA is wise
#                    - gap characters permitted
#                    - tabs + spaces forbidden
#           
#           3  line: "+"
#                    - can included a full repeat of the title line text
#                    - default no
#                    
#           4  line: quality
#                    - no wrapped (but can be)
#                    - subset of printable ASCII characters 33-126 inclusive
#                    - quality string must be equal in length to the sequence
#                      string.
#                    - WARNIG: "@" character can compare anywhere in the quality#                      string (start)

#           attention to newline characters:
#                      Unix style - line feed only, ASCII 10
#                      Win/DOS - carriage return and line feed, ASCII 13 then 10

#           footer:    #Sanger FASTQ


# suggerimenti:
#         - possibile copiare da PairedFastaQualIterator() in the Bio.SeqIO.QualityIO module but:
#           usa iteratori di biopython molto lenti -> restituiscono oggetti seqrecord
#           seq e qual devono contenere stessi id


#         - possibile usare  MultipleBlockStreamingReader(object) + efficiente di
#           /home/michele/bioinfotree/binary/Michele-Desktop/local/lib/python/vfork/fasta/reader.py per leggere blocchi fasta. Ma funziona anche per leggere i fasta qual?


# testing:
#       uso le sequenze in nar-02248-d-2009-File005







# Code:

# argument parser
import argparse
# system-specific parameters
# and function
import sys
# module for reading and writing FASTQ and QUAL format files as SeqRecord objects
from Bio.SeqIO.QualityIO import QualPhredIterator, FastqPhredWriter
from Bio.SeqIO.FastaIO import FastaIterator
from Bio.Alphabet import single_letter_alphabet
from PipeUtils import PipeUtil
   
def main(arguments):

    sys.argv = arguments
    # parse command line arguments
    args = arg_parse()

    fq_writer = FastqPhredWriter(sys.stdout)
    fq_writer.write_header()

    fasta_iter = FastaIterator(open(args.fasta_file, "r"), alphabet=single_letter_alphabet, \
                                   title2ids=None)

    if args.hash_sort == True:
        util = PipeUtil()
        q_dic = util.get_qual_index(args.qual_file)

        while True: 
            try: 
                f_rec = fasta_iter.next() 
            except StopIteration:
                f_rec = None
            if f_rec is None: 
                  #End of fasta file
                  break

#############################################              
            try:
                q_rec = q_dic[f_rec.id]
            except:
                idValErr()

#############################################
            
            if len(f_rec) != len(q_rec.letter_annotations["phred_quality"]):
                lenValErr(f_rec.id)
            #Merge the data.... 
            f_rec.letter_annotations["phred_quality"] = q_rec.letter_annotations["phred_quality"]
            # write to std-out
            fq_writer.write_record(f_rec)


    else:
        qual_iter = QualPhredIterator(open(args.qual_file, "r"), alphabet=single_letter_alphabet, \
                                    title2ids=None)

        while True: 
            try: 
                f_rec = fasta_iter.next() 
            except StopIteration: 
                f_rec = None 
            try: 
                q_rec = qual_iter.next() 
            except StopIteration: 
                q_rec = None 
            if f_rec is None: 
                  #End of fasta file
                  break

#############################################
              
            # error if there is no qual for a fasta entry
            if q_rec is None and f_rec is not None: 
                idValErr()

            if f_rec.id != q_rec.id:
                raise ValueError("FASTA and QUAL entries do not match (%s vs %s)." \
                                 % (f_rec.id, q_rec.id))

#############################################

            
            if len(f_rec) != len(q_rec.letter_annotations["phred_quality"]):
                lenValErr(f_rec.id)
            #Merge the data.... 
            f_rec.letter_annotations["phred_quality"] = q_rec.letter_annotations["phred_quality"]
            # write to std-out
            fq_writer.write_record(f_rec)

    fq_writer.write_footer()
    
    return 0




def lenValErr(id):
    raise ValueError("Sequence length and number of quality scores disagree for %s" % id)
    return 0

def idValErr():
    raise ValueError("FASTA file has more entries than the QUAL file.")
    return 0







def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="Converts a FASTA file and the corresponding Phred quality files in a FASTQ-SANGER file format. The QUAL file must contain at least the same IDs as the FASTA file. The 2 files should be ordered sequentially according to the IDs in order to reduce computation time. The results are thrown to stdout.", prog=sys.argv[0])
    parser.add_argument("--version", "-v", action="version", version="%(prog)s 0.1")
    parser.add_argument("--fasta", "-f", dest="fasta_file", required=True, help="Fasta file")
    parser.add_argument("--qual", "-q", dest="qual_file", required=True, help="Phred quality file")
    parser.add_argument("--hash_sort", "-u", dest="hash_sort", required=False, action="store_true", help="Assume sequentially disordered and pick up quality by in-memory hashing (for small datasets)")
    args = parser.parse_args()
    
    return args



# Is invoked when the module is executed
if __name__ == "__main__":
    # Explicitly pass command line arguments
    # because required vor invoke main from
    # other script
    main(sys.argv)
# 
# fastaqual2fastq.py ends here
