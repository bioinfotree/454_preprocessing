#! /usr/bin/perl

use strict;
use warnings;
use List::Util qw(sum min max);
use Getopt::Long;
use File::Basename;

# separator is:
$,="\t";

$SIG{__WARN__} = sub {die @_};


# Parameter variables
my $helpAsked;
my $total;


# test if no argument are given or
# if no file is providen to STDIN
if ((!@ARGV) && (-t STDIN)) {
    print "$0: unexpected argument number.\n";
    prtUsage();
    exit 1
}



GetOptions(
    "t|total" => \$total, # flag
    "h|help" => \$helpAsked
    ) or die;



# test if option is given
if(defined($helpAsked)) {
	prtUsage();
	exit 1;
}


my $prevFastaSeqId = "";
my $fastaSeqId = "";
my $fastaSeq = "";
my $seqCount = 0;
my $ttlQual = 0;


# read to STDIN
while(my $line = <STDIN>) {
	chomp $line;
	if($line =~ /^>/) {
		$line =~ s/^>//;
		$prevFastaSeqId = $fastaSeqId;
		$fastaSeqId = $line;
		if($fastaSeq ne "") {
			prtQuality($prevFastaSeqId, $fastaSeq);
		}
		$fastaSeq = "";
	}
	else {
		$fastaSeq .= $line . " ";
	}
}
if($fastaSeq ne "") {
	$prevFastaSeqId = $fastaSeqId;
	prtQuality($prevFastaSeqId, $fastaSeq);
}


if(defined($total)) 
{
    printf (STDOUT "%0.4f\n", $ttlQual/$seqCount);
}

exit 0;



sub prtQuality {
	my $id = $_[0];
	my $qualStr = $_[1];
	chop $qualStr;
	$seqCount++;
	my @qVal = split(/\s+/, $qualStr);
	checkQ($id, \@qVal);
	my $sum = sum(@qVal);
	my $qual = sprintf "%0.4f", $sum/(scalar @qVal);
	$ttlQual += $qual;
	if(!defined($total)) 
	{
	    print (STDOUT "$id\t$qual\n");	
	}
}




#test phred values
sub checkQ
{
    my($id, $qVal) = @_;
    foreach (@{$qVal}) 
    {
	if ($_ < 0 || $_ >= 100)
	{
	    prtError(sprintf "Ivalid PHRED value %i in block %s. Range [0-100].", $_, $id);
	};
    } 

}


sub prtHelp {
	print "\nOptions:\n";
	print "  -h, --help\n";
	print "    Prints this help and exit.\n";
	print "  -t, --total\n";
	print "    Returns only mean PHREAD quality for whole blocks.\n";
	print "\n";
	print " FASTA_QUAL blocks from STDIN\n";
	print "\n";
	print " Results to STDOUT\n";
	print "\n";
}

sub prtError {
	my $msg = $_[0];
	printf STDERR "\n$msg\n";
	exit 1;
}

sub prtUsage {
	print "\nUsage: $0 [OPTIONS] <FASTA_QUAL\n";
	print "\n";
	print "Computes mean PHRED quality of every FASTA_QUAL blocks in input, ";
	print "or mean PHRED quality for whole blocks.\n";
	prtHelp();
}
